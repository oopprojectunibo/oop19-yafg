 /**
 * This Class represent a full game object with all the Views and Controllers instantiated.
 */
package application;

import java.io.FileNotFoundException;
import controller.CharacterView;
import controller.MatchController;
import controller.animations.AnimatedImage;
import controller.attack.AttackController;
import controller.player_controller.PlayerController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import model.Background;
import model.Character;
import model.Floor;

public class Game {
    private Stage stage;
    private Group root;
    private Scene scene;
    private Canvas canvas;
    private GraphicsContext gc;
    private AnimatedImage animation1;
    private AnimatedImage animation2;
    private final Background background;
    private final Floor floor;
    private final Character player1;
    private final Character player2;
    private CharacterView cv1 = null;
    private CharacterView cv2 = null;
    private MatchController st1 = null;
    private MatchController st2 = null;
    private AttackController atk1 = null;
    private AttackController atk2 = null;
    private PlayerController ctrl1 = null;
    private PlayerController ctrl2 = null;
    private final long startNanoTime;

    /**
     * The class constructor, it associate the parameters to the attributes and launch the other essentials objects building.
     * @param stage the game Stage
     * @param root the game Group
     * @param scene the game Scene
     * @param canvas the game Canvas
     * @param gc the game GraphicsContext to draw the Characters
     * @param background the game background
     * @param floor the game Floor
     * @param player1 the first Character
     * @param player2 the second Character
     * @param startNanoTime the start time
     */
    public Game(final Stage stage, final Group root, final Scene scene, final Canvas canvas, final GraphicsContext gc, final Background background, final Floor floor, final Character player1, final Character player2, final long startNanoTime) {
       this.stage = stage;
       this.setRoot(root);
       this.scene = scene;
       this.setCanvas(canvas);
       this.gc = gc;
       this.background = background;
       this.floor = floor;
       this.player1 = player1;
       this.player2 = player2;
       this.startNanoTime = startNanoTime;
       this.createAnimation();
       this.createCharactersView();
       this.createSettingsControllers();
       this.createAttackControllers();
       this.createPlayerControllers();
    }
    /**
     * This method instantiate the the two AnimatedImage objects, one for every Character.
     */
    private void createAnimation() {
        try {
            this.animation1 = new AnimatedImage(this.player1, this.player1.getId());
            this.animation2 = new AnimatedImage(this.player2, this.player2.getId());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    /**
     * This method instantiate the the two CharacterView using the AnimatedImages.
     */
    private void createCharactersView() {
        this.cv1 = new CharacterView(this.gc, this.animation1);
        this.cv2 = new CharacterView(this.gc, this.animation2);
    }
    /**
     * This method instantiate the the two Settings controller.
     */
    private void createSettingsControllers() {
       this.st1 = new MatchController(this.cv1, this.floor);
        this.st2 = new MatchController(this.cv2, this.floor);
    }
    /**
     * This method instantiate the the two AttackControllers.
     */
    private void createAttackControllers() {
        this.atk1 = new AttackController(this.player1, this.player2, this.st1);
        this.atk2 = new AttackController(this.player2, this.player1, this.st2);
    }
    /**
     * This method instantiate the the two PlayerControllers and connect them to the scene.
     */
    private void createPlayerControllers() {
       this.ctrl1 = new PlayerController(player1, KeyCode.A, KeyCode.D, KeyCode.W, KeyCode.S, KeyCode.C, KeyCode.V, KeyCode.B, this.atk1);
       this.ctrl2 = new PlayerController(player2, KeyCode.LEFT, KeyCode.RIGHT, KeyCode.UP, KeyCode.DOWN, KeyCode.I, KeyCode.O, KeyCode.P, this.atk2);
       this.ctrl1.register(this.scene);
       this.ctrl2.register(this.scene);
    }

    public final Character getPlayer1() {
        return player1;
    }

    public final Character getPlayer2() {
        return player2;
    }

    public final MatchController getSettings1() {
        return st1;
    }

    public final MatchController getSettings2() {
        return st2;
    }

    public final long getStartNanoTime() {
        return startNanoTime;
    }

    public final Stage getStage() {
        return stage;
    }

    public final Background getBackground() {
        return background;
    }

    public final Floor getFloor() {
        return floor;
    }

    public final GraphicsContext getGraphicsContext() {
        return gc;
    }

    public final Canvas getCanvas() {
        return canvas;
    }

    public final void setCanvas(final Canvas canvas) {
        this.canvas = canvas;
    }

    public final Group getRoot() {
        return root;
    }

    public final void setRoot(final Group root) {
        this.root = root;
    }
}
