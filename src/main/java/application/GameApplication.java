package application;

import java.util.List;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.Stage;
import model.MediaSound;

public class GameApplication extends Application {

    List<String> characters;
    String background;

    public GameApplication(List<String> characters, String background) {
        this.characters = characters;
        this.background = background;
    }

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("YAFG: Nuova Partita");
        MainController mc = new MainController(this.characters, this.background, stage);
        AnimationTimer at = mc.getGameStage();
                
        if(at!=null) {
            at.start();
            stage.show();
        }else {
            MediaSound.Sounds.MUSIC_MENU.stopLastMediaPlayer();
            try {
                new Main().start(new Stage());
                stage.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            stage.close();
            this.stop();
        }
        System.out.println("launched");
        stage.show();
    }  

}
