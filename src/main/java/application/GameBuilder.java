/**
 * This class build a Game object with the essentials Objects that are a Stage with the others graphics component,
 * the two Characters with the health and fury bars, the Floor and the background. 
 * @author Luca Tonelli
 */
package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.Background;
import model.Character;
import model.Floor;

public final class GameBuilder {
    /**
     * These constants represent the Scene Height and Width.
     */
    public static final double W = 1280, H = 800;
    /**
     * This constant represent the spawn distance from the border.
     */
    public static final double SPAWN_DISTANCE = 50;
    private Stage stage;
    private Group root;
    private Scene scene;
    private Canvas canvas;
    private GraphicsContext gc;
    private Background background = null;
    private Floor floor = null;
    private Character player1 = null;
    private Character player2 = null;
    private long startNanoTime = 0;
    /**
     * This attribute is true if all the build methods doesn't throws exceptions.
     */
    private boolean integrity = true;

    /**
     * This method instantiate the stage and other graphics components with the right parameter.
     * @param stage the game stage
     */
    public GameBuilder(final Stage stage) {
        this.stage = stage;
        this.stage.setTitle("YAFG Fight");
        try {
            stage.getIcons().add(new Image(new FileInputStream("src/main/resources/ico/ico.png")));
        } catch (FileNotFoundException e) {
            this.integrity = false;
        }
        if(!this.integrity) {
            this.integrity = true;
            try {
                stage.getIcons().add(new Image(new FileInputStream("../../src/main/resources/ico/ico.png")));
            } catch (FileNotFoundException e) {
                this.integrity = false;
            }
        }
        this.root = new Group();
        this.scene = new Scene(root);
        this.stage.setScene(scene);
        this.canvas = new Canvas(W, H);
        this.root.getChildren().add(this.canvas);
        this.gc = canvas.getGraphicsContext2D();
        this.stage.setResizable(false);
    }

    /**
     * This method is the builder start method that launch the constructor method.
     * @param stage the game stage
     * @return the GameBuilder for other buildings 
     */
    public static GameBuilder newBuilder(final Stage stage) {
        return new GameBuilder(stage);
    }

    /**
     * This method try to build the background using setBackground.
     * @param path
     * @return the GameBuilder for other buildings 
     */
    public GameBuilder background(final String path) {

        try {
            this.setBackground("src/main/resources/background/" + path + "/background.png");
           
        } catch (NullPointerException e) {
            this.integrity = false;
        }
        if(!integrity) {
            try {
                this.setBackground("../../src/main/resources/background/" + path + "/background.png");
            }  catch (NullPointerException e) {
                this.integrity = false;
            }
        }
       
        return this;
    }
    
    /**
     * Build the background.  
     * @param path the full image path
     * @throws FileNotFoundException
     * @throws NullPointerException
     */
    private void setBackground(final String path) throws NullPointerException{
        Image imageBackground = null;
        try {
            imageBackground = new Image(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            this.integrity = false;
        }
        this.background = new Background(0, 0, imageBackground);
        
    }

    /**
     * This method try to build the floor using setFloor.
     * @param path the name of the floor in the path
     * @return the GameBuilder for other buildings 
     */
    public GameBuilder floor(final String path) {
        try {
           this.setFloor("src/main/resources/background/" + path + "/floor.png");
        } catch (NullPointerException e) {
            this.integrity = false;
        }
        
        if (!this.integrity) {
            this.integrity=true;
            try {
                this.setFloor("../../src/main/resources/background/" + path + "/floor.png");
              
             } catch (NullPointerException e) {
                 this.integrity = false;
             }  
        }
        return this;
    }
    
    /**
     * Build the Floor.  
     * @param path the full image path
     * @throws FileNotFoundException
     * @throws NullPointerException
     */
    private void setFloor(final String path) {
        Image imgFloor;
        try {
            imgFloor = new Image(new FileInputStream(path));
            this.floor = new Floor(imgFloor);
            this.floor.setWidth(W);
            this.floor.setHeight(floor.getImage().getHeight());
            this.floor.setY(H - floor.getImage().getHeight());
        } catch (FileNotFoundException e) {
            this.integrity = false;
        }
        
        
    }

    /**
     * This method build the two characters with the respective health and fury bars. 
     * @param li a list of id to build (in this case the list can only generate two characters)
     * @return the GameBuilder for other buildings 
     */

    public GameBuilder characters(final List<String> li) {

        int index = 0;
        Image img1 = null;
        Image img2 = null;
        try {
            img1 = this.controlCharacterImage("src/main/resources/characters/player" + li.get(index) + "/idle/Idle_000.png");
            index++;
            img2 = this.controlCharacterImage("src/main/resources/characters/player" + li.get(index) + "/idle/Idle_000.png");
        } catch (NullPointerException e) {
            this.integrity = false;
        }
        
        if(this.integrity==false) {
           this.integrity=true;
           index = 0;
           try {
               img1 = this.controlCharacterImage("../../src/main/resources/characters/player" + li.get(index) + "/idle/Idle_000.png");
               index++;
               img2 = this.controlCharacterImage("../../src/main/resources/characters/player" + li.get(index) + "/idle/Idle_000.png");
           } catch (NullPointerException e) {
               this.integrity = false;
           }
        }
        if(this.integrity) {
            index=0;
            try {
                this.player1 = new Character(img1, true, li.get(index));
            } catch (FileNotFoundException e) {
                
            }
            this.player1.setX(SPAWN_DISTANCE);
            this.player1.setY(Floor.FLOOR_LINE - this.player1.getImageHeight());
            root.getChildren().add(this.player1.getBar().getProgressBar().getHealthRectangle());
            root.getChildren().add(this.player1.getBar().getProgressBar().getFuryRectangle());
            this.player1.getBar().updateFury(0);

            index++;

            try {
                this.player2 = new Character(img2, false, li.get(index));
            } catch (FileNotFoundException e) {
               
            }
            this.player2.setX(W - SPAWN_DISTANCE);
            this.player2.setY(Floor.FLOOR_LINE - this.player2.getImageHeight());
            root.getChildren().add(this.player2.getBar().getProgressBar().getHealthRectangle());
            root.getChildren().add(this.player2.getBar().getProgressBar().getFuryRectangle());
            this.player2.getBar().updateFury(0);
        }
        return this;
    }
    
    /**
     * Instantiante the Character image.  
     * @param path the full image path
     * @throws FileNotFoundException
     * @throws NullPointerException
     */
    private Image controlCharacterImage(final String path) {
        FileInputStream fi;
        Image img1 = null;
        try {
            fi = new FileInputStream(path);
            img1 = new Image(fi);
        } catch (FileNotFoundException e) {
            this.integrity = false;
        }
        
        return img1;
        
    }

    /**
     * This method instantiate the start game time to the System.nanoTime() value. 
     * @return the GameBuilder for other buildings 
     */
    public GameBuilder setAnimationTimer() {
        this.startNanoTime = System.nanoTime();
        return this;

    }

    /**
     * This method build a Game object using the previous builded parameter.
     * @return a Game object if the integrity boolean is true else null
     */
    public Game build() {
        if (this.integrity) {
            return new Game(this.stage, this.root, this.scene, this.canvas, this.gc, this.background, this.floor, this.player1, this.player2, this.startNanoTime);
        }
        return null;
    }
}
