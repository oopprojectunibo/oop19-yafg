package application;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import controller.MainMenuController;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.MediaSound;
import model.MediaSound.Sounds;
import view.factory.GameViewFactoryFX;


/**
 * This class represent the Main class of the JavaFX-based application.
 */
public final class Main extends Application {
    @Override
    public void start(final Stage stage) throws Exception {

        MediaSound.Sounds.MUSIC_MENU.playMedia(Sounds.MUSIC_MENU);
        stage.setTitle("YAFG");
        
        try {
            stage.getIcons().add(new Image(new FileInputStream("../../src/main/resources/ico/ico.png")));
        } catch (FileNotFoundException e) {
            stage.getIcons().add(new Image(new FileInputStream("src/main/resources/ico/ico.png")));
        }
        stage.show();
        MainMenuController controller = new MainMenuController(new GameViewFactoryFX(stage), stage);
    }

    public static URL getFXMLURL(final String fxmlFile) {
        return ClassLoader.getSystemResource("layouts/" + fxmlFile + ".fxml");
    }
    
    public static URL getURL(final String url) {
        return ClassLoader.getSystemResource(url);
    }


    /**
     * 
     * @param args
     *                 unused
     */
    public static void main(final String[] args) {
        launch();

    }

}
