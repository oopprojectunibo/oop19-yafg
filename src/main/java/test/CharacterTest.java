package test;
/**
 * @author Giacomo Grassetti
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javafx.scene.image.Image;
import model.Character;

class CharacterTest {

    @Test
    void testId() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        String result = c1.getId();
        Assertions.assertEquals("0", result);
    }

    @Test
    void testDefSpeed() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        double result = c1.getDefaultSpeed();
        Assertions.assertEquals(350, result);
    }

    @Test
    void testIndicatorBar() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        double result = c1.getBar().getHealthCapacity();
        Assertions.assertEquals(100, result);
        result = c1.getBar().getFuryCapacity();
        Assertions.assertEquals(0, result);
    }

    @Test
    void testJumpSpeed() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        double result = c1.getJumpSpeed();
        Assertions.assertEquals(6, result);
    }

    @Test
    void testIsUpIsDown() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        c1.setUp(true);
        boolean result = c1.isUp();
        Assertions.assertEquals(true, result);
        c1.setJumping(false);
        Assertions.assertEquals(false, c1.isUp());
    }

    @Test
    void testLightAttack() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        boolean result = c1.isLightAttacking();
        Assertions.assertEquals(false, result);
        c1.setLightAttacking(true);
        Assertions.assertEquals(true, c1.isLightAttacking());
    }

    @Test
    void testStrongAttack() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        boolean result = c1.isStrongAttacking();
        Assertions.assertEquals(false, result);
        c1.setStrongAttacking(true);
        Assertions.assertEquals(true, c1.isStrongAttacking());
    }

    @Test
    void testSpecialMove() throws FileNotFoundException {
        Character c1 = new Character(new Image(new FileInputStream("src/main/resources/characters/player0/idle/Idle_000.png")), true, "0");
        boolean result = c1.isSpecialMoving();
        Assertions.assertEquals(false, result);
        c1.setSpecialAttack(true);
        Assertions.assertEquals(true, c1.isSpecialMoving());
    }

}
