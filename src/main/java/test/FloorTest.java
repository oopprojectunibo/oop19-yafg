package test;
/**
 * @author Giacomo Grassetti
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javafx.scene.image.Image;
import model.Floor;


public class FloorTest {
    @Test
    void testHeight() throws FileNotFoundException {
       Floor fl = new Floor(new Image(new FileInputStream("src/main/resources/floor/test.png")));
       fl.setHeight(30);
       Assertions.assertEquals(30, fl.getHeight());
    }

    @Test
    void testWidht() throws FileNotFoundException {
       Floor fl = new Floor(new Image(new FileInputStream("src/main/resources/floor/test.png")));
       fl.setWidth(100);
       Assertions.assertEquals(100, fl.getWidth());
    }
}
