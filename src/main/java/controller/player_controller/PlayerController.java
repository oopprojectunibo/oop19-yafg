/**
 * A class representing the controller associated to each player currently present on screen
 * @author Lorenzo Massone
 * @version 2.0
 */

package controller.player_controller;

import controller.attack.AttackController;
import controller.attack.AttackController.Type;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import model.Character;

public class PlayerController {

    private final Character player;
    private final AttackController atk;
    private final KeyCode leftKey;
    private final KeyCode rightKey;
    private final KeyCode upKey;
    private final KeyCode downKey;
    private final KeyCode lightAtk;
    private final KeyCode strongAtk;
    private final KeyCode specialMove;
    private final static double lightAtkCooldown = 0.7;
    private final static double strongAtkCooldown = 1.0;
    private long lastUpdateLight = 0;
    private long lastUpdateStrong = 0;
    private double elapsedSecondsLight = 1.0;
    private double elapsedSecondsStrong = 1.0;
    private double elapsedNanoSecondsLight = 0.0;
    private double elapsedNanoSecondsStrong = 0.0;

    public PlayerController(Character player, KeyCode leftKey, KeyCode rightKey, KeyCode upKey, KeyCode downKey, KeyCode lightAtk,
            KeyCode strongAtk, KeyCode specialMove, AttackController atk) {
        this.player = player;
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.downKey = downKey;
        this.lightAtk = lightAtk;
        this.strongAtk = strongAtk;
        this.specialMove = specialMove;
        this.atk = atk;
    }

    /**
     * The method used to handle keyboard inputs and tie them to the Scene.
     * 
     * @param scene
     *                  used to catch inputs for the character controller
     *                  istantiated
     */
    public void register(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            if (e.getCode() == leftKey) {
                player.setLeft(true);
                player.setOrientation(false);
            }
            if (e.getCode() == rightKey) {
                player.setRight(true);
                player.setOrientation(true);
            }
            if (e.getCode() == upKey) {
                player.setUp(true);
            }
            if (e.getCode() == downKey)
                player.setDown(true);
            if (e.getCode() == lightAtk) {
                this.updateLightAtkTimer();
                if (canLightAtk()) {
                    player.setLightAttacking(true);
                    this.atk.attack(Type.Light);
                    this.updateLightAtkTimer();
                }
            }
            if (e.getCode() == strongAtk) {
                this.updateStrongAtkTimer();
                if (canStrongAtk()) {
                    player.setStrongAttacking(true);
                    this.atk.attack(Type.Strong);
                    this.updateStrongAtkTimer();
                }
            }
            if (e.getCode() == this.specialMove) {
                System.out.println("Special press");
                this.atk.attack(Type.Special);
            }
        });

        scene.addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (e.getCode() == leftKey) {
                player.setLeft(false);
            }
            if (e.getCode() == rightKey) {
                player.setRight(false);
            }
            if (e.getCode() == upKey) {
                player.setUp(false);
            }
            if (e.getCode() == downKey) {
                player.setDown(false);
            }
        });
    }

    /**
     * A method that controls if the character can do a light attack.
     * 
     * @return true if the time elapsed is enough to make to the character unable to
     *         "spam" attacks, otherwise false.
     */
    private boolean canLightAtk() {
        if (elapsedSecondsLight >= lightAtkCooldown) {
            return true;
        }
        return false;
    }

    /**
     * A method that controls if the character can do a strong attack.
     * 
     * @return true if the time elapsed is enough to make to the character unable to
     *         "spam" attacks, otherwise false.
     */
    private boolean canStrongAtk() {
        if (elapsedSecondsStrong >= strongAtkCooldown) {
            return true;
        }
        return false;
    }

    /**
     * A method to update elapsedSeconds, representings the seconds from the last attack.
     * Fundamental component of the cooldown attack system.
     */
    private void updateLightAtkTimer() {
        if (lastUpdateLight == 0) {
            lastUpdateLight = System.nanoTime();
            return;
        }
        elapsedNanoSecondsLight = (double) System.nanoTime() - lastUpdateLight;
        elapsedSecondsLight = elapsedNanoSecondsLight / 1_000_000_000.0;
        if (player.isLightAttacking()) {
            lastUpdateLight = System.nanoTime();
        }
    }

    /**
     * A method to update elapsedSeconds, representings the seconds from the last strong attack.
     * Fundamental component of the cooldown attack system.
     */
    private void updateStrongAtkTimer() {
        if (lastUpdateStrong == 0) {
            lastUpdateStrong = System.nanoTime();
            return;
        }
        elapsedNanoSecondsStrong = (double) System.nanoTime() - lastUpdateStrong;
        elapsedSecondsStrong = elapsedNanoSecondsStrong / 1_000_000_000.0;
        if (player.isStrongAttacking()) {
            lastUpdateStrong = System.nanoTime();
        }
    }

}