/**
 * This class represent the view of a character; is used to print the correct character frame 
 * according to the update method parameters 
 * @author Luca Tonelli
 */
package controller;


import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import controller.animations.AnimatedImage;
import controller.MatchController.MOVESET;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import model.Pair;


public class CharacterView {

    private final GraphicsContext gc;
    private final AnimatedImage animation;

    public CharacterView(final GraphicsContext gc, final  AnimatedImage animation) {
        this.gc = gc;
        this.animation = animation;

    }

    /**
     * This is the main method that is used to identify what frame the controller want to print.
     * 
     * @param move represent the movement that the controller want to print
     * @param position is the character position 
     * @param orientation define if the frame orientation should be left or right 
     * @param elapsedSeconds the seconds passed from last main cycle
     * @param index if present represent the attack frame to print
     * @return the new image height and width
     */
    public List<Double> update(final MOVESET move, final Pair<Double, Double> position, final boolean orientation, final double elapsedSeconds, final Optional<Integer> index) {
        Image frame = null;
        int id = 0;
        List<Double> li = new LinkedList<>();
        switch (move) {
            case Light:
                if (index.isPresent()) {
                    id = index.get();
                    frame = animation.getLightAttackFrame(id);
                }
                break; 
            case Strong:
                if (index.isPresent()) {
                    id = index.get();
                    frame = animation.getStrongAttackFrame(id);
                }
                break;
            case Jump:
                frame = animation.getJumpFrame(elapsedSeconds);
                break;
            case Walk:
                frame = animation.getWalkingFrame(elapsedSeconds);
                break;
            case Idle:
                frame = animation.getIdleFrame(elapsedSeconds);
                break;
            case Dead:
                frame = animation.getDeadFrame(elapsedSeconds);
                break;
            default:
                frame = animation.getIdleFrame(elapsedSeconds);
                break;
        }

        li.add(0, (frame.isError()) ? (double) 0 : frame.getHeight());
        li.add(1, (frame.isError()) ? (double) 0 : frame.getHeight());
        this.printCharacter(frame, position, orientation);
        return li;
    }

    /**
     * This is the printer method it launch different method due to the orientation.
     * @param frame is the right image to print
     * @param position is the position of the image
     * @param orientation is the image orientation 
     */
    public void printCharacter(final Image frame, final Pair<Double, Double> position, final boolean orientation) {
        if (orientation) {
            this.gc.drawImage(frame, position.getX(), position.getY());
        } else {
            this.gc.drawImage(frame, position.getX() + MatchController.COLLISION_BOUNDS, position.getY(), -(frame.getWidth()), frame.getHeight());
        }
    }

   /**
    * This method return the number of frame from attack.
    * @param value is the attack that need to know the number of frame
    * @return number of frames of the chosen attack 
    */
    public int getAnimationSize(final MOVESET value) {
        switch (value) {
            case Light:
                return this.animation.getLightAtkSize();
            case Strong:
                return this.animation.getStrongAtkSize();
            default:
                return 0;

        }
    }
}
