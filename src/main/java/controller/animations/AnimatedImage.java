/**
 * A class which manages to use of the correct images during attacks and other movements.
 * Attacks and movements work on different principles: 
 * attacks are indipendent from the elapsed time since when an attack key is pressed all the animation has to be performed,
 * while movements depend on the elapsed time since the last cycle of the AnimationTimer
 * @author Lorenzo Massone
 */
package controller.animations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javafx.scene.image.Image;
import model.Character;

public class AnimatedImage {

    /**
     * All the list of frames that should be displayed for different actions.
     */
    private List<Image> idle;
    private List<Image> walk;
    private List<Image> lightAtk;
    private List<Image> strongAtk;
    private List<Image> jump;
    private List<Image> dead;
    //private String relPath = "../../";
    private String relPath = "";
    private String idlePath = "src/main/resources/characters/player%d/idle/Idle_00%d.png";
    private String walkPath = "src/main/resources/characters/player%d/walk/Walk_00%d.png";
    private String lightAtkPath = "src/main/resources/characters/player%d/attack/Attack_00%d.png";
    private String strongAtkPath = "src/main/resources/characters/player%d/strong_attack/Strong_Attack_00%d.png";
    private String jumpPath = "src/main/resources/characters/player%d/jump/Jump_00%d.png";
    private String deathPath = "src/main/resources/characters/player%d/dead/Dead_00%d.png";
    private Character player;
    private double duration = 0.075;

    public AnimatedImage(final Character player1, final String id) throws FileNotFoundException {
        this.player = player1;
        initImageList(id);
    }

    /**
     * Gets the right walking frame.
     * 
     * @param time are the elapsed seconds from the first launch of the AnimationTimer
     * @return the walking frame
     */
    public Image getWalkingFrame(final double time) {
        int index = (int) ((time % (walk.size() * duration)) / duration);
        return walk.get(index);
    }

    /**
     * Gets the right idle state frame.
     * 
     * @param time are the elapsed seconds from the first launch of the AnimationTimer
     * @return the idle frame
     */
    public Image getIdleFrame(final double time) {
        int index = (int) ((time % (idle.size() * duration)) / duration);
        return idle.get(index);
    }

    /**
     * Gets the right idle state frame.
     * 
     * @param time are the elapsed seconds from the first launch of the AnimationTimer
     * @return the dead frame
     */
    public Image getDeadFrame(final double time) {
        int index = (int) ((time % (dead.size() * duration)) / duration);
        return dead.get(index);
    }

    /**
     * Gets the right attack state frame.
     * 
     * @param id is the index of the correct frame
     * @return the right attack frame
     */
    public Image getLightAttackFrame(final int id) {
        try {
            if (id >= getLightAtkSize()) {
                this.player.setLightAttacking(false);
            } else {
                this.player.setLightAttacking(true);
            }
            return lightAtk.get(id);
        } catch (IndexOutOfBoundsException e) {
        }
        return null;
    }

    /**
     * Gets the number of frames of the Light Attack.
     * 
     * @return the number of frames
     */
    public int getLightAtkSize() {
        return this.lightAtk.size();
    }

    /**
     * Gets the right attack state frame.
     * 
     * @param id
     *               is the index of the correct frame
     * @return the correct StrongAttack frame
     */
    public Image getStrongAttackFrame(final int id) {
        try {
            if (id >= getStrongAtkSize()) {
                this.player.setStrongAttacking(false);
            } else {
                this.player.setStrongAttacking(true);
            }
            return strongAtk.get(id);
        } catch (IndexOutOfBoundsException e) {
        }
        return null;
    }

    /**
     * Gets the number of frames of the Strong Attack.
     * 
     * @return the number of frames
     */
    public int getStrongAtkSize() {
        return this.strongAtk.size();
    }

    /**
     * Gets the right jump state frame.
     * 
     * @param time are the elapsed seconds from the first launch of the AnimationTimer
     * @return the jump frame
     */
    public Image getJumpFrame(final double time) {
        int index = (int) ((time % (jump.size() * duration)) / duration);
        return jump.get(index);
    }

    /**
     * A method to return to player associated with this AnimatedImage.
     * 
     * @return the player
     */
    public Character getPlayer() {
        return this.player;
    }

    /**
     * A method which uses Streams to format all the Strings given in a List and
     * initialize the list of frames.
     * 
     * @param list
     *                 is the list containing all the paths of the images describing
     *                 a type of move for player that is going to be initialized
     * @param id
     *                 is the ID of the player that is going to be initialized
     * @return the List of Images that have to be used
     */
    private List<Image> initImage(final List<String> list, final String id) {
        List<Image> result;
        result = IntStream.range(0, list.size() - 1).mapToObj(x -> String.format(list.get(x), Integer.parseInt(id), x)).map(y -> {
            try {
                return new Image(new FileInputStream(y));
            } catch (FileNotFoundException e) {
                try {
                    return new Image(new FileInputStream(relPath.concat(y)));
                } catch (FileNotFoundException e1) {
                }
            }
            return null;
        }).collect(Collectors.toList());
        return result;
    }

    /**
     * A method to initialize the list of images necessary to animate the many
     * actions of the player.
     * 
     * @param id
     *               is the number representing the player that we want to
     *               initialize
     * @throws FileNotFoundException
     *                                   if the method cannot find the Images
     */
    public void initImageList(final String id) throws FileNotFoundException {
        this.idle = initImage(
                Collections.nCopies(new File(this.relPath + "src/main/resources/characters/player" + id + "/idle").listFiles().length, idlePath),
                id);
        this.walk = initImage(
                Collections.nCopies(new File(this.relPath + "src/main/resources/characters/player" + id + "/walk").listFiles().length, walkPath),
                id);
        this.lightAtk = initImage(Collections
                .nCopies(new File(this.relPath + "src/main/resources/characters/player" + id + "/attack").listFiles().length, lightAtkPath), id);
        this.strongAtk = initImage(Collections.nCopies(
                new File(this.relPath + "src/main/resources/characters/player" + id + "/strong_attack").listFiles().length, strongAtkPath), id);
        this.jump = initImage(
                Collections.nCopies(new File(this.relPath + "src/main/resources/characters/player" + id + "/jump").listFiles().length, jumpPath),
                id);
        this.dead = initImage(Collections
                .nCopies(new File(this.relPath + "src/main/resources/characters/player" + id + "/dead").listFiles().length, deathPath), id);
    }

}
