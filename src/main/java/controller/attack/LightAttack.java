package controller.attack;

/**
 * @author Lorenzo Massone
 * @author Giacomo Grassetti
 */
import javafx.scene.paint.Color;
import model.Character;
import model.MediaSound;
import model.MediaSound.Sounds;

public class LightAttack extends AttackDecorator {

    public LightAttack(final Character c1, final Character c2) {
        super(c1, c2, Damage.LIGHT);
    }


    @Override
    public void hit() {
        if (!AttackController.collisionsControlerRight(c1, c2) || !AttackController.collisionsControlerLeft(c1, c2)) {
            MediaSound.Sounds.L_HIT.playSound(Sounds.L_HIT);
            c2.getBar().updateHealth(this.damage);
            c1.getBar().updateFury(10);
            c2.getBar().updateFury(15);
            if (c1.getBar().getFuryCapacity() >= 100) {
                c1.getBar().getProgressBar().getFuryRectangle().setFill(Color.DARKORANGE);
            }
            if (c2.getBar().getFuryCapacity() >= 100) {
                c2.getBar().getProgressBar().getFuryRectangle().setFill(Color.DARKORANGE);
            }
        }
    }

}
