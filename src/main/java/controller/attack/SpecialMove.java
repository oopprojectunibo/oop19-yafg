/**
 * This class represent the special move of any character, that is the attack unlocked when the fury is full
 * @ Luca Tonelli
 */
package controller.attack;
/**
 * @author Lorenzo Massone
 * @author Giacomo Grassetti
 *
 */

import model.Character;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

public class SpecialMove {

    private Character c1;
    private Character c2;

    public SpecialMove(Character c1, Character c2) {
        this.c1 = c1;
        this.c2 = c2;
    }

    /**
     * This method is used to define the special move of any character
     * @param id the character id
     * @return a Service that implement the special move of the selected Character
     */
    public Service<Void> getService(String id) {
        switch (id) {
        case "0":
            return this.getNinjaBoySpecialMove();
        case "1":
            return this.getCowgirlSpecialMove();
        case "2":
            return this.getNinjaGirlSpecialMove();
        case "3":
            return this.getRoboSpecialMove();
        default:
            return this.getNinjaBoySpecialMove();
        }
        
    }
    
    /**
     * This service duplicate the speed of Character that launch it for 10 seconds 
     * @return the ninja boy special move 
     */
    private Service<Void> getNinjaBoySpecialMove(){
        return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() throws Exception {
                        final double speed = c1.getSpeed();
                        c1.setSpeed(speed * 2);
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException interrupted) {
                            if (isCancelled()) {
                                c1.setSpeed(c1.getDefaultSpeed());
                                updateMessage("Cancelled");
                            }
                        }
                        c1.setSpeed(c1.getDefaultSpeed());
                        return null;

                    }
                };
            }
        };
    }
    
    /**
     * This service decrease the speed of the enemy Character of 1/3 for 5 seconds 
     * @return the ninja girl special move 
     */
    private Service<Void> getNinjaGirlSpecialMove(){
        return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() throws Exception {

                        final double speed = c2.getSpeed();
                        c2.setSpeed(speed / 3);
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException interrupted) {
                            if (isCancelled()) {
                                c2.setSpeed(c2.getDefaultSpeed());
                                updateMessage("Cancelled");
                            }
                        }
                        c2.setSpeed(c2.getDefaultSpeed());
                        return null;

                    }
                };
            }
        };
    }
    
    /**
     * This service increase the health of the launcher Character of actual health * 1,7 
     * @return the Cowgirl special move 
     */
    private Service<Void> getCowgirlSpecialMove(){
        return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() throws Exception {

                        int actual = c1.getBar().getHealthCapacity();
                        c1.getBar().increaseHealth((int) (actual * 1.7));
                        return null;

                    }
                };
            }
        };
    }
    
    /**
     * This service hit 1 damage to the enemy Character every 170ms if the launcher Character is in collision with the enemy for 10s
     * @return the robot special move 
     */
    private Service<Void> getRoboSpecialMove(){
        return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    protected Void call() throws Exception {
                        ContactAttack robotHit = new ContactAttack(c1, c2);
                        long t = System.currentTimeMillis();
                        long end = t + 10000;
                        while (System.currentTimeMillis() < end) {
                            robotHit.hit();
                            try {
                                Thread.sleep(170);
                            } catch (InterruptedException interrupted) {
                                if (isCancelled()) {
                                    updateMessage("Cancelled");
                                }
                            }
                        }
                        return null;
                    }
                };
            }
        };
    }
    
    
}
