package controller.attack;
import javafx.scene.paint.Color;
/**
 * @author Lorenzo Massone
 * @author Giacomo Grassetti
 */
import model.Character;
import model.MediaSound;
import model.MediaSound.Sounds;

public class StrongAttack extends AttackDecorator {

    public StrongAttack(final Character c1, final Character c2) {
        super(c1, c2, Damage.STRONG);
    }


    @Override
    public final void hit() {
        if (!AttackController.collisionsControlerLeft(c1, c2) || !AttackController.collisionsControlerRight(c1, c2)) {
            MediaSound.Sounds.S_HIT.playSound(Sounds.S_HIT);
            c2.getBar().updateHealth(super.getDamage());
            c1.getBar().updateFury(10);
            c2.getBar().updateFury(15);
            if (c1.getBar().getFuryCapacity() >= 100) {
                c1.getBar().getProgressBar().getFuryRectangle().setFill(Color.DARKORANGE);
            }
            if (c2.getBar().getFuryCapacity() >= 100) {
                c2.getBar().getProgressBar().getFuryRectangle().setFill(Color.DARKORANGE);
            }
        }
    }
}
