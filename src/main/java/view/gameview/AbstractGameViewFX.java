/**
 * This abstract {@link GameView} uses JavaFX.
 * The default update method does nothing.
 */
package view.gameview;

import application.Main;
import javafx.fxml.FXMLLoader;

public abstract class AbstractGameViewFX implements GameView {

    private FXMLLoader loader;

    public AbstractGameViewFX(final ViewLayouts layout) {
        this.loader = new FXMLLoader(Main.getFXMLURL(layout.name().toLowerCase()));
    }

    /**
     * @return the {@link FXMLLoader} the implementation should use to change the Stage
     */
    public final FXMLLoader getFXMLLoader() {
        return this.loader;
    }

    @Override
    public void update() { }
}
