package view.gamefield;

/**
 * this is the view class of Indicator.
 * @author Giacomo Grassetti
 * @version 1.4
 */
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class IndicatorView {
    // create a progressbar
    private Rectangle healthbar;
    private Rectangle furybar;

    /**
     * IndicatorView Constructor's.
     * 
     * @param width
     *                    is the width of the Indicator
     * @param heigh
     *                    is the height of the Indicator
     * @param color
     *                    is the color of the Indicator
     * @param player1
     *                    true if is the first player.
     */
    public IndicatorView(final double width, final double heigh, final Paint color, final boolean player1) {
        if (player1) {
            this.healthbar = new Rectangle(width, heigh, color);
            this.furybar = new Rectangle(0, heigh, width, heigh);
            this.furybar.setFill(Color.BLUE);
        } else {
            this.healthbar = new Rectangle(application.GameBuilder.W - width, 0, 100, heigh);
            this.healthbar.setFill(Color.RED);
            this.furybar = new Rectangle(application.GameBuilder.W - width, heigh, 0, heigh);
            this.furybar.setFill(Color.BLUE);
        }
    }

    /**
     * @return healthbar
     */
    public Rectangle getHealthRectangle() {
        return this.healthbar;
    }

    /**
     * @return furybar
     */
    public Rectangle getFuryRectangle() {
        return this.furybar;
    }
}
