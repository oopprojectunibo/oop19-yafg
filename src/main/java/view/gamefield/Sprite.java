/**
 * Represents an image intended to be drawn on a screen in a specific position.
 */

package view.gamefield;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Sprite {

    private double x, y, width, height;
    private Image image;
    private ImageView imageView;

    public Sprite(final double x, final double y, final Image image) {
        this.x = x;
        this.y = y;
        this.image = image;
        this.imageView = new ImageView(image);
        this.height = imageView.getFitHeight();
        this.width = imageView.getFitWidth();
    }

    /**
     * @return width
     */
    public double getWidth() {
        return width;
    }

    /**
     * set Image width.
     * @param width the width
     */
    protected void setWidth(final double width) {
        this.width = width;
    }

    /**
     * @return height
     */
    public double getHeight() {
        return height;
    }

    /**
     * set height.
     * @param height the height
     */
    protected void setHeight(final double height) {
        this.height = height;
    }

    /**
     * @return Image width
     */
    public double getImageWidth() {
        return this.image.getWidth();
    }

    /**
     * @return Image height
     */
    public double getImageHeight() {
        return this.image.getHeight();
    }

    public final void setX(final double x) {
        this.x = x;
    }

    public final void setY(final double y) {
        this.y = y;
    }

    protected final void setImage(final Image image) {
        this.image = image;
    }

    public final double getX() {
        return x;
    }

    public final double getY() {
        return y;
    }

    public final Image getImage() {
        return this.image;
    }

    protected final ImageView getImageView() {
        return this.imageView;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((image == null) ? 0 : image.hashCode());
        long temp;
        temp = Double.doubleToLongBits(x);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Sprite other = (Sprite) obj;
        if (image == null) {
            if (other.image != null) {
                return false;
            }
        } else if (!image.equals(other.image)) {
            return false;
        }
        if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }

}
