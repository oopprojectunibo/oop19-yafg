/** 
 * This class represents the the playground floor consisting of an image, a position (x,y), height and width.
 * Getter and Setter included.
 * @author Lorenzo Massone
 * @author Luca Tonelli
 * @version 1.1 
 */
package model;

import java.io.FileNotFoundException;
import javafx.scene.image.Image;
import view.gamefield.Sprite;

public class Floor extends Sprite {
    /**
     * 
     */
    public static final int FLOOR_LINE = (int) application.GameBuilder.H - 185; 

    /**
     * Constructor 
     * Set position (x,y), height and width.
     * @param image required
     * @throws FileNotFoundException if path is not valid.
     */
    public Floor(final Image image) throws FileNotFoundException {
         super(0, 0, image);

         //setting the fit height and width of the image view 
         this.getImageView().setFitHeight(300);

         //Setting the preserve ratio of the image view 
         this.getImageView().setPreserveRatio(true);  
    }


    /** @return width of the Floor */
    public double getWidth() {
        return getImageView().getFitWidth();
    }


    /** @return height of the Floor */
    public double getHeight() {
        return getImageView().getFitHeight();
    }


    /**
     * Set the Height of the Floor.
     */
    @Override
    public final void setHeight(final double value) {
        getImageView().setFitHeight(value);
    }
    
    
    /**
     * Set the Width of the Floor
     */
    @Override
    public final void setWidth(final double value) {
        getImageView().setFitWidth(value);
    }

    
}
