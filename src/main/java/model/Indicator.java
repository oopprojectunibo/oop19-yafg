package model;

/**
 * This class represents the model of Indicator Bar of life and fury in the game.
 * @author Giacomo Grassetti
 * @version 1.6 
 * 
 */
import javafx.scene.paint.Color;
import view.gamefield.IndicatorView;

public class Indicator {

    public static final int MAX_CAPACITY = 100;
    public static final int MIN_CAPACITY = 0;

    private boolean dead = false;
    public IndicatorView progressBar;
    private int currentHealthCapacity;
    private int currentFuryCapacity;

    /**
     * Indicator Costructor's
     * 
     * @param player1
     *                    true if is the first player.
     */
    public Indicator(boolean player1) {
        this.currentHealthCapacity = 100;
        this.currentFuryCapacity = 0;
        this.progressBar = new IndicatorView(100, 30, Color.RED, player1);
    }

    /**
     * get current health capacity
     * 
     * @return int: currentHealtCapacity
     */
    public int getHealthCapacity() {
        return this.currentHealthCapacity;
    }

    /**
     * get current fury capacity
     * 
     * @return int: currentFuryCapacity
     */
    public int getFuryCapacity() {
        return this.currentFuryCapacity;
    }

    /**
     * Set the capacity of the HealthBar
     * 
     * @param num
     *                is the capacity
     */
    public void setHealthCapacity(int num) {
        this.currentHealthCapacity = num;
    }

    /**
     * Increases health
     * 
     * @param num:
     *                 value to add
     */
    public void increaseHealth(int num) {
        if (this.currentHealthCapacity + num >= MAX_CAPACITY) {
            this.currentHealthCapacity = MAX_CAPACITY;
        }
        this.currentHealthCapacity += num;
        this.progressBar.getHealthRectangle().setWidth(this.currentHealthCapacity);
    }

    /**
     * Sets the fury capacity
     * 
     * @param num
     *                is the num that has to be sets
     */
    public void setFuryCapacity(int num) {
        this.currentFuryCapacity += num;
    }

    /**
     * reset fury to 0
     */
    public void resetFury() {
        this.currentFuryCapacity = MIN_CAPACITY;
    }

    /**
     * get the progress bar
     * 
     * @return IndicatorView
     */
    public IndicatorView getProgressBar() {
        return this.progressBar;
    }

    /**
     * Update health value after damage
     * 
     * @param damage:
     *                    value of damage
     */
    public void updateHealth(int damage) {
        if (this.currentHealthCapacity - damage <= MIN_CAPACITY) {
            this.dead = true;
        } else {
            this.currentHealthCapacity -= damage;
            this.progressBar.getHealthRectangle().setWidth(this.currentHealthCapacity);
            this.dead = false;
        }
    }

    /**
     * Update Fury after damage or attack
     * 
     * @param num:
     *                 value to increase
     */
    public void updateFury(int num) {
        if (this.currentFuryCapacity < MAX_CAPACITY) {
            this.currentFuryCapacity += num;
            this.progressBar.getFuryRectangle().setWidth(this.currentFuryCapacity);
        }
    }

    /**
     * @return true if player is dead
     */
    public boolean isDead() {
        return this.dead;
    }

}
